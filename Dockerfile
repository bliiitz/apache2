FROM ubuntu:18.04

RUN apt-get update &&  apt-get upgrade -y && apt-get install -y curl apt-transport-https apache2

RUN rm -rf /var/lib/apt/lists/* && \
    rm -rf /etc/apache2/sites-available/* && \
    rm -rf /var/www/*

ENV APACHE_RUN_USER=www-data \
    APACHE_RUN_GROUP=www-data \
    APACHE_LOG_DIR=/var/log/apache2 \
    APACHE_LOCK_DIR=/var/lock/apache2 \
    APACHE_PID_FILE=/var/run/apache2.pid 

RUN curl -Lo /usr/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 && chmod +x /usr/bin/dumb-init
RUN chown -R www-data /var/www

RUN a2enmod headers

EXPOSE 80
ENTRYPOINT ["dumb-init", "--rewrite", "15:28", "--"]
